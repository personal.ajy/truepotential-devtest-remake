# TruePotential Development Test (Remake)
This is a remake of a deevlopment test I completed for True Potential.

## Overview
A simple ticket system that allows either an admin or a client to use it. Clients can log in, create tickets, view their own tickets on their dashboard, view replies to their tickets and post new replies. Admins can log in and view a dashboard listing all submitted tickets, can post replies to tickets and mark them as resolved/closed.

## Technology
I have used the following technologies:
1. **VB** as my back-end language
2. **JavaScript** with **jQuery** for my front-end
3. **ASP.NET MVC** as my website framework
4. **Stored Procedures** to get data from my database
5. **SQL Server 2014** for data storage
6. **Bootstrap** for styling
7. **FluentValidation** for server-side data validation