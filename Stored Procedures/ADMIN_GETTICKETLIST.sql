CREATE PROCEDURE [dbo].[ADMIN_GETTICKETLIST]
AS
    SELECT  t.ID ,
            t.DESCRIPTION ,
            u.FORENAME + ' ' + u.SURNAME AS CREATED_BY_NAME ,
            t.CREATED_DATE ,
            u2.FORENAME + ' ' + u2.SURNAME AS CLAIMED_BY_NAME ,
            t.STATUS ,
            ts.DESCRIPTION AS STATUS_NAME
    FROM    TICKETS t
            LEFT JOIN USERS u ON t.CREATOR_USER_ID = u.ID
            LEFT JOIN USERS u2 ON t.CLAIMED_USER_ID = u2.ID
            LEFT JOIN dbo.CODE_TICKETS_STATUS ts ON ts.CODE = t.STATUS
    ORDER BY t.CREATED_DATE DESC;
            

