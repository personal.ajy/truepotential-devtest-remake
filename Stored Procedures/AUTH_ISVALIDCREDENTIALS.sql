CREATE PROCEDURE AUTH_ISVALIDCREDENTIALS
    @LOGIN VARCHAR(100) ,
    @PASSWORD VARCHAR(100)
AS
    BEGIN
        DECLARE @USER_ID UNIQUEIDENTIFIER;

        IF EXISTS ( SELECT TOP 1
                            ID
                    FROM    USERS
                    WHERE   LOGIN = @LOGIN )
        BEGIN
            SELECT 1
            FROM   dbo.USERS
            WHERE  LOGIN = @LOGIN
			AND PASSWORD_HASHED = HASHBYTES('SHA2_512',
                                        @PASSWORD
                                        + CAST(PASSWORD_SALT AS NVARCHAR(36)))
		END
    END;

