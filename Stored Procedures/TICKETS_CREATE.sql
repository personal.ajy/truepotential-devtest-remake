CREATE PROCEDURE TICKETS_CREATE
    @DESCRIPTION AS VARCHAR(100) ,
    @CREATOR_USER_ID AS UNIQUEIDENTIFIER ,
    @BODY AS VARCHAR(MAX)
AS
    DECLARE @ID AS UNIQUEIDENTIFIER = NEWID();
    DECLARE @CREATED_DATE AS DATETIME = GETDATE();

    INSERT  INTO dbo.TICKETS
            ( ID ,
              DESCRIPTION ,
              CREATOR_USER_ID ,
              CREATED_DATE ,
              STATUS ,
              CLAIMED_USER_ID ,
              CLOSED_DATE ,
              SATISFACTION_RATING ,
              BODY
            )
    VALUES  ( @ID , -- ID - uniqueidentifier
              @DESCRIPTION , -- DESCRIPTION - varchar(100)
              @CREATOR_USER_ID , -- CREATOR_USER_ID - uniqueidentifier
              @CREATED_DATE , -- CREATED_DATE - datetime
              0 , -- STATUS - int
              NULL , -- CLAIMED_USER_ID - uniqueidentifier
              NULL , -- CLOSED_DATE - datetime
              0 , -- SATISFACTION_RATING - int
              @BODY  -- BODY - varchar(max)
            );

    SELECT  @ID AS ID ,
            @DESCRIPTION AS DESCRIPTION ,
			@CREATOR_USER_ID AS CREATOR_USER_ID,
            @CREATED_DATE AS CREATED_DATE ,
            0 AS STATUS ,
            NULL AS CLAIMED_USER_ID ,
            NULL AS CLOSED_DATE ,
            0 AS SATISFACTION_RATING ,
            @BODY AS BODY;


