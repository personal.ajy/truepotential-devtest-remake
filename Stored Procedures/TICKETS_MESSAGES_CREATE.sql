CREATE PROCEDURE TICKETS_MESSAGES_CREATE
	@TICKET_ID UNIQUEIDENTIFIER,
	@CREATOR_USER_ID UNIQUEIDENTIFIER,
	@BODY VARCHAR(MAX)
AS

	DECLARE @MESSAGE_ID UNIQUEIDENTIFIER = NEWID()
	DECLARE @CREATED_DATE DATETIME = GETDATE()

	INSERT INTO dbo.TICKETS_MESSAGES
	        ( ID ,
	          TICKET_ID ,
	          BODY ,
	          CREATOR_USER_ID ,
	          CREATED_DATE
	        )
	VALUES  ( @MESSAGE_ID , -- ID - uniqueidentifier
	          @TICKET_ID , -- TICKET_ID - uniqueidentifier
	          @BODY , -- BODY - varchar(max)
	          @CREATOR_USER_ID , -- CREATOR_USER_ID - uniqueidentifier
	          @CREATED_DATE  -- CREATED_DATE - datetime
	        )
	
    SELECT  @MESSAGE_ID AS ID,
            @TICKET_ID AS TICKET_ID,
            @BODY AS BODY,
            @CREATOR_USER_ID AS CREATOR_USER_ID,
            @CREATED_DATE AS CREATED_DATE;

