CREATE PROCEDURE [dbo].[USERS_CREATE]
    @FORENAME VARCHAR(50) ,
    @SURNAME VARCHAR(50) ,
    @LOGIN VARCHAR(100) ,
    @PASSWORD VARCHAR(20) ,
    @IS_ADMIN BIT = 0
AS
    BEGIN
		
        DECLARE @SALT UNIQUEIDENTIFIER = NEWID();
        DECLARE @ID UNIQUEIDENTIFIER = NEWID() ,
            @CREATED_DATE DATE = GETDATE() ,
            @PASSWORD_HASHED VARBINARY(64) = HASHBYTES('SHA2_512',
                                                       @PASSWORD
                                                       + CAST(@SALT AS NVARCHAR(36)));

        INSERT  INTO dbo.USERS
                ( ID ,
                  LOGIN ,
                  PASSWORD_HASHED ,
                  PASSWORD_SALT ,
                  CREATED_DATE ,
                  FORENAME ,
                  SURNAME ,
                  IS_ADMIN
		        )
        VALUES  ( @ID ,
                  @LOGIN ,
                  @PASSWORD_HASHED ,
                  @SALT ,
                  @CREATED_DATE ,
                  @FORENAME ,
                  @SURNAME ,
                  @IS_ADMIN
		        );

        SELECT  @ID AS ID ,
                @LOGIN AS LOGIN ,
                @PASSWORD_HASHED AS PASSWORD_HASHED ,
                @FORENAME AS FORENAME ,
                @SURNAME AS SURNAME ,
                @IS_ADMIN AS IS_ADMIN;
    END;

