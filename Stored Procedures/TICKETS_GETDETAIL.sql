CREATE PROCEDURE [dbo].[TICKETS_GETDETAIL]
    @TICKET_ID UNIQUEIDENTIFIER
AS
    SELECT  *
    INTO    #ticket
    FROM    dbo.TICKETS
    WHERE   ID = @TICKET_ID;

    SELECT  *
    INTO    #documents
    FROM    dbo.TICKETS_DOCUMENTS
    WHERE   TICKET_ID = @TICKET_ID;

    SELECT  *
    INTO    #messages
    FROM    dbo.TICKETS_MESSAGES
    WHERE   TICKET_ID = @TICKET_ID
    ORDER BY CREATED_DATE ASC;

	-- Get ticket details
    SELECT  #ticket.* ,
            dbo.CODE_TICKETS_STATUS.DESCRIPTION AS STATUS_NAME ,
            u1.FORENAME + ' ' + u1.SURNAME AS CREATOR_FULLNAME,
			u2.FORENAME + ' ' + u2.SURNAME AS CLAIMED_USER_FULLNAME
    FROM    #ticket
            JOIN dbo.CODE_TICKETS_STATUS ON #ticket.STATUS = dbo.CODE_TICKETS_STATUS.CODE
            LEFT JOIN dbo.USERS u1 ON #ticket.CREATOR_USER_ID = u1.ID
			LEFT JOIN dbo.users u2 ON #ticket.CLAIMED_USER_ID = u2.ID

	-- Get documents
    SELECT  *
    FROM    #documents;

	-- Get messages
    SELECT  #messages.* ,
            USERS.FORENAME + ' ' + USERS.SURNAME AS CREATOR_FULLNAME,
			USERS.IS_ADMIN AS CREATOR_IS_ADMIN
    FROM    #messages
            JOIN dbo.USERS ON USERS.ID = #messages.CREATOR_USER_ID
			ORDER BY #messages.CREATED_DATE ASC;

