"Clearing existing files..."

Get-ChildItem -Path "Stored Procedures/" -Include *.* -File -Recurse | foreach { $_.Delete()}

Write-Output "Finding SP's"

function exec-dbcmd([string] $cmdtext) {
    $server = "."
    $db = "TPDEVTEST"

    $connstring = "Server=$server; Database=$db; Integrated Security=True"
    $conn = New-Object System.Data.SqlClient.SqlConnection
    $conn.ConnectionString = $connstring

    $cmd = new-object System.Data.SqlClient.SqlCommand
    $cmd.CommandText =$cmdtext
    $cmd.Connection = $conn

    $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
    $SqlAdapter.SelectCommand = $cmd
    $DataSet = New-Object System.Data.DataSet
    $SqlAdapter.Fill($DataSet) | Out-Null
    $conn.Close() 

    return $DataSet
}

$ds = exec-dbcmd("SELECT name, 
            type
     FROM dbo.sysobjects
     WHERE (type = 'P')")

"Downloading..."

foreach ($row in $ds.Tables[0].Rows) {
    $name = $row["name"]
    $ds2 = exec-dbcmd("EXEC sp_helptext '$name'")
    $str = ""
    foreach ($row in $ds2.Tables[0].Rows) {
        $str += $row["Text"]
    }

    $str | Out-File "Stored Procedures/$name.sql" -Force
}

"Done"

exit