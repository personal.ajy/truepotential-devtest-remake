﻿@ModelType List(Of Models.Message)

@For Each msg In Model
    @<div class="message">
        <div class="message-headerbar">
            <span>
                By <strong>@msg.CreatorFullName</strong> (on <i>@msg.CreatedDate.ToString()</i>)
                @If msg.CreatorIsAdmin Then
                    @<span>(Admin)</span>
                End If
            </span>
        </div>
        <div Class="message-content">
            @Html.Raw(msg.Body)
        </div>
    </div>
Next