﻿@ModelType Models.ViewModels.FeedbackViewModel

<div class="alert alert-@Model.Type.ToString().ToLower()" alert-dismissible" role="alert">
    @If Model.CanClose Then
        @<button type="button" Class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    End If
    <h4>@Model.Title</h4>
    <p>@Model.Text</p>

    @If Not String.IsNullOrEmpty(Model.HtmlBody) Then
        @<div>
            @Html.Raw(Model.HtmlBody)
        </div>
    End If

    @If Model.StrList IsNot Nothing Then
        @<hr />
        @<ul>
            @For Each item In Model.StrList
                @<li>@item.ToString()</li>
            Next
        </ul>
    End If
</div>
