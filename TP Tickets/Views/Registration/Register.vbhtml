﻿@ModelType TP_Tickets.Models.ViewModels.RegistrationViewModel
@Code
    ViewData("Title") = "Register"
    Layout = "~/Views/Shared/Layouts/_Main.vbhtml"
End Code

@Using (Html.BeginForm("ActionRegister", "Registration", FormMethod.Post))
    @<div>
        <div class="form-group">
            @Html.LabelFor(Function(m) m.Forename)
            @Html.TextBoxFor(Function(m) m.Forename, New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(m) m.Forename)
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(m) m.Surname)
            @Html.TextBoxFor(Function(m) m.Surname, New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(m) m.Surname)
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(m) m.Login))
            @Html.TextBoxFor(Function(m) m.Login, New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(m) m.Login)
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(m) m.Password)
            @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(m) m.Password)
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(m) m.PasswordRepeated, "Password (repeated)")
            @Html.PasswordFor(Function(m) m.PasswordRepeated, New With {.class = "form-control"})
            @Html.ValidationMessageFor(Function(m) m.PasswordRepeated)
        </div>

        <hr />

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Register" />
        </div>
    </div>
End Using
