﻿@ModelType Models.ViewModels.AdminCPTicketDetailsViewModel
@Code
    ViewData("Title") = "Details"
    Layout = "~/Views/Shared/Layouts/_Main.vbhtml"
End Code

@Section Scripts
    <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
End Section

@Section SecondaryNav
    <nav class="navbar navbar-default navbar-static-top navbar-small">
        <div class="container">
            <ul class="nav navbar-nav navbar-left">
                @If Model.Ticket.ClaimedUserId Is Nothing Then
                    @<li><a class="navbar-item-small" onclick="claimTicket()">Claim</a></li>
                Else
                    If Model.Ticket.ClaimedUserId = Helpers.CurrentUser.Id Then
                        @<li><a class="navbar-item-small" onclick="releaseTicket()" style="cursor:pointer;">Release</a></li>
                    End If
                End If

                @If Model.Ticket.ClosedDate Is Nothing Then
                    @<li> <a Class="navbar-item-small" style="cursor: pointer;">Close</a></li>
                Else
                    @<li><a class="navbar-item-small" style="cursor:pointer;">Open</a></li>
                End If

                <li> <a Class="navbar-item-small" href="">Delete</a></li>
            </ul>

            <ul Class="nav navbar-nav navbar-right"></ul>
        </div>
    </nav>
End Section

<div>
    <div Class="col-md-7 nopadding">
        <h2>@Model.Ticket.Description<br /><small>(@Model.Ticket.Id)</small></h2>
    </div>

    <div class="col-md-4 nopadding">
        <h4>Creator <small>@Model.Ticket.CreatorFullName</small></h4>
        <h4>Created Date <small>@Model.Ticket.CreatedDate.ToString()</small></h4>
        <h4>Status <small>@Model.Ticket.Status.ToString()</small></h4>
    </div>
</div>

<br />

<div style="top:   50px; position: relative;">
    <h3>Description</h3>
    <div class="ticket-body-container">@Html.Raw(Model.Ticket.Body)</div>

    <hr />

    <div>
        <h3>
            Messages
            @If Model.Ticket.Messages.Count > 0 Then
                @<small>@Model.Ticket.Messages.Count</small>
            End If
        </h3>

        <div id="messages">
            @If Model.Ticket.Messages IsNot Nothing AndAlso Model.Ticket.Messages.Count > 0 Then
                @Html.Partial("~/Views/Partials/TicketMessages.vbhtml", Model.Ticket.Messages)
            Else
                @<p>There are currently no messages for this ticket.</p>
            End If
        </div>


        <div style="margin-top: 30px; position: relative;">
            <div class="form-group">
                <textarea id="txtareaReplyContent"></textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" id="btnSubmitMessage" onclick="submitMessage()">Submit</button>
            </div>
        </div>
    </div>

    <hr />

    <div>
        <h3>Documents</h3>
        @If Model.Ticket.Documents IsNot Nothing AndAlso Model.Ticket.Documents.Count > 0 Then

        Else
            @<p>There are currently no documents for this ticket.</p>
        End If
    </div>
</div>

@Section BodyScripts
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            }
        });

        function submitMessage() {
            var content = tinymce.activeEditor.getContent();

            if (content == null || content.length < 3) {
                alert("Please ensure your reply is more than 3 characters long!");
            } else {
                $.ajax({
                    method: "post",
                    url: "/Ticket/AddMessage",
                    data: {
                        "ticketId": "@Model.Ticket.Id",
                        "body": content
                    },
                    success: function (data) {
                        tinymce.activeEditor.setContent("");

                        $.ajax({
                            method: "post", url: "/Ticket/GetMessagesPartialView",
                            data: { "ticketId": "@Model.Ticket.Id" },
                            success: function (resp) {
                                $("#messages").html(resp);
                            },
                            error: function (ex) {
                                alert("There was an error reloading the messages for this ticket");
                            }

                        });
                    },
                    error: function (ex) {
                        alert("There was an error submitting your response!");
                    }
                });
            }
        }

        function claimTicket() {
            var ticketId = '@Model.Ticket.Id'

            $.ajax({
                method: "post", url: "@Url.Action("Claim", "AdminCP")",
                data: { "ticketId": ticketId },
                success: function (resp) {
                    $("#feedback-container").html(resp);
                },
                error: function (ex) {
                    $("#feedback-container").html(ex.responseText);
                }
            });
        }

        function releaseTicket() {
            var ticketId = '@Model.Ticket.Id'

            $.ajax({
                method: "post", url: "@Url.Action("Release", "AdminCP")",
                data: { "ticketId": ticketId },
                success: function (resp) {
                    $("#feedback-container").html(resp);
                },
                error: function (ex) {
                    $("#feedback-container").html(ex.responseText);
                }
            });
        }
    </script>
End Section
