﻿@ModelType Models.ViewModels.AdminCPViewModel
@Code
    ViewData("Title") = "Index"
    Layout = "~/Views/Shared/Layouts/_Main.vbhtml"
End Code

<div class="col-xs-7">
    <h3>Information</h3>
    <p>All tickets on this system are shown in the table below. You can hover your mouse over each ticket ID to reveal the full ID.</p>
</div>

<div class="col-xs-3">
    <h3>Statistics</h3>

    <p>
        <span><strong>Total tickets:</strong> @Model.Tickets.Count</span><br />
        <span><strong>Closed tickets:</strong></span>
    </p>
</div>

<div style="top: 30px; position: relative;">
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Description</th>
                <th>Created By</th>
                <th>Created Date</th>
                <th>Claimed By</th>
                <th>Status</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            @For Each ticket In Model.Tickets
            @<tr>
                <td onmouseover="showTicketId(this)"
                    onmouseout="hideTicketId(this)"
                    ticketid="@ticket.Id.ToString()">
                    @ticket.Id.ToString().Substring(0, 8)...
                </td>
                <td>@ticket.Description</td>
                <td>@ticket.CreatedByName</td>
                <td>@ticket.CreatedDate</td>
                <td>@IIf(ticket.ClaimedByName Is Nothing, "(none)", ticket.ClaimedByName)</td>
                <td>@ticket.StatusName</td>
                <td>
                    <a href="@String.Format("/AdminCP/Ticket/Details/" + ticket.Id.ToString())">View</a>
                </td>
            </tr>
            Next
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function showTicketId(td) {
        $td = $(td)
        $td.html($td.attr("ticketid"));
    }

    function hideTicketId(td) {
        $td = $(td)
        $td.html($td.attr("ticketid").substring(0, 8) + "...");
    }
</script>