﻿@ModelType TP_Tickets.Models.ViewModels.LoginViewModel
@Code
    ViewData("Title") = "Login"
    Layout = "~/Views/Shared/Layouts/_Main.vbhtml"
End Code

@Using (Html.BeginForm("Login", "Authentication", FormMethod.Post))
    @<div>
        <div class="form-group">
            @Html.LabelFor(Function(x) x.Login)
            @Html.TextBoxFor(Function(x) x.Login, New With {.class = "form-control"})
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(x) x.Password)
            @Html.PasswordFor(Function(x) x.Password, New With {.class = "form-control"})
        </div>

        <hr />

        <div class="form-group">
            <input type="submit" value="Login" class="btn btn-primary" />
        </div>
    </div>
End Using
