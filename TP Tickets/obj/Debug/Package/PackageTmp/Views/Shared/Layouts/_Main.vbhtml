﻿<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />

    <title>@ViewData("Title")</title>

    <script src="@Url.Content("~/Scripts/jquery-1.9.1.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
    <script src="@Url.Content("~/Scripts/bootstrap.min.js")" type="text/javascript"></script>
    @RenderSection("Scripts", False)

    <link href="@Url.Content("~/Content/bootstrap.min.css")" rel="stylesheet" type="text/css" />
    <link href="@Url.Content("~/Content/Site/main.css")" rel="stylesheet" type="text/css" />
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">TruePotential Tickets</a>
            </div>

            <ul class="nav navbar-nav navbar-left">
                @if Helpers.IsUserLoggedIn Then
                    @<li><a href="@Url.Action("Create", "Ticket")">New Ticket</a></li>
                    @<li><a>My Tickets</a></li>

                    If Helpers.CurrentUser.IsAdmin Then
                        @<li><a href="@Url.Action("Index", "AdminCP")">Tickets (Admin)</a></li>
                        @<li><a href="">Closed Tickets (Admin)</a></li>
                    End If
                End If
            </ul>

            <ul Class="nav navbar-nav navbar-right">
                @If Helpers.IsUserLoggedIn Then
                    @<li><a>Logged in as <b>@Helpers.CurrentUser.Login</b></a></li>
                    @<li><a href="@Url.Action("Logout", "Authentication")">Logout</a></li>
                Else
                    @<li><a href="@Url.Action("Login", "Authentication")">Login</a></li>
                End If
            </ul>

        </div>
    </nav>

    <div>
        @RenderSection("SecondaryNav", False)
    </div>

    <div Class="center-content">
        <div>
            <div id="feedback-container">
                @If TempData("Feedback") IsNot Nothing Then
                @Html.Partial("~/Views/Partials/Feedback.vbhtml", CType(TempData("Feedback"), Models.ViewModels.FeedbackViewModel))
                End If
            </div>

            @RenderBody()
        </div>
    </div>

    <div id="popup"></div>

    @RenderSection("BodyScripts", False)
</body>
</html>