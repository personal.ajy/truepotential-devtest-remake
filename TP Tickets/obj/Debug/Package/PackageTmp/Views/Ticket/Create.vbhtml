﻿@ModelType Models.ViewModels.CreateTicketViewModel
@Code
    ViewData("Title") = "Create"
    Layout = "~/Views/Shared/Layouts/_Main.vbhtml"
End Code

@Section Scripts
    <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
End Section

<h4>Create a new ticket</h4>
<p>To create a new ticket, please provide a title and a description of your issue. When you are done, click the 'Create' button at the bottom of the form to submit it.</p>

@Using (Html.BeginForm("Create", "Ticket", FormMethod.Post, New With {.id = "form"}))
    @<div Class="form-group">
        @Html.LabelFor(Function(x) x.Description)
        @Html.TextBoxFor(Function(x) x.Description, New With {.class = "form-control"})
        @Html.ValidationMessageFor(Function(x) x.Description)
    </div>

    @<div Class="form-group">
        @Html.LabelFor(Function(x) x.Body)
        @Html.TextArea("textareaBody", New With {.class = "form-control", .style = "height: 250px;", .id = "txtareaBody"})
        @Html.ValidationMessageFor(Function(x) x.Body)
    </div>

    @Html.HiddenFor(Function(x) x.Body, New With {.id = "hiddenBody"})

    @<div class="form-group">
        <input type="button" class="btn btn-primary" value="Create" id="btnSubmit"/>
    </div>
End Using

<script type="text/javascript">
    tinymce.init({
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

    $("#btnSubmit").on("click", function () {
        var txt = tinymce.activeEditor.getContent();
        $("#hiddenBody").val(txt);
        $("#form")[0].submit();
    });
</script>