﻿Imports System.Data.SqlClient
Imports TP_Tickets.Models

Namespace Data
    Public Class UserData
        Public Shared Function GetIdFromLogin(login As String) As Guid
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "USERS_GETIDFROMLOGIN"

                cmd.Parameters.AddWithValue("@LOGIN", login)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim row = ds.Tables(0).Rows(0)

                    Return CType(row("ID"), Guid)
                End If
            End Using

            Return Nothing
        End Function

        Public Shared Function GetDetails(Optional id As Guid? = Nothing, Optional login As String = Nothing) As UserDetails
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "USERS_GETDETAILS"

                cmd.Parameters.AddWithValue("@LOGIN", login)
                cmd.Parameters.AddWithValue("@ID", id)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim row = ds.Tables(0).Rows(0)

                    Dim userDetails = New UserDetails
                    userDetails.Id = CType(row("ID"), Guid)
                    userDetails.Forename = CStr(row("FORENAME"))
                    userDetails.Surname = CStr(row("SURNAME"))
                    userDetails.Login = CStr(row("LOGIN"))
                    userDetails.IsAdmin = CBool(row("IS_ADMIN"))

                    Return userDetails
                End If
            End Using

            Return Nothing
        End Function
    End Class
End Namespace
