﻿Imports System.Data.SqlClient
Imports TP_Tickets.Models

Namespace Data
    Public Class AdminCPData
        Public Shared Function GetTicketList() As List(Of TicketListItem)
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "ADMIN_GETTICKETLIST"

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim ticketListItems As New List(Of TicketListItem)

                    For Each row As DataRow In ds.Tables(0).Rows
                        Dim ticketListItem = New TicketListItem
                        ticketListItem.Id = CType(row("ID"), Guid)
                        ticketListItem.Description = CStr(row("DESCRIPTION"))
                        ticketListItem.CreatedDate = CDate(row("CREATED_DATE"))
                        ticketListItem.CreatedByName = CStr(row("CREATED_BY_NAME"))
                        ticketListItem.ClaimedByName = If(IsDBNull(row("CLAIMED_BY_NAME")), Nothing, CStr(row("CLAIMED_BY_NAME")))
                        ticketListItem.Status = CType(row("STATUS"), Ticket.TicketStatus)
                        ticketListItem.StatusName = CStr(row("STATUS_NAME"))

                        ticketListItems.Add(ticketListItem)
                    Next

                    Return ticketListItems
                End If
            End Using

            Return Nothing
        End Function

        Public Shared Sub ClaimTicket(ticketId As Guid, userId As Guid)
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "ADMIN_CLAIMTICKET"

                cmd.Parameters.AddWithValue("@TICKET_ID", ticketId)
                cmd.Parameters.AddWithValue("@USER_ID", userId)

                connection.Open()

                cmd.ExecuteNonQuery()
            End Using
        End Sub

        Public Shared Sub ReleaseTicket(ticketId As Guid)
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "ADMIN_RELEASETICKET"

                cmd.Parameters.AddWithValue("@TICKET_ID", ticketId)

                connection.Open()

                cmd.ExecuteNonQuery()
            End Using
        End Sub
    End Class
End Namespace
