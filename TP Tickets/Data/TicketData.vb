﻿Imports System.Data.SqlClient
Imports TP_Tickets.Models

Namespace Data
    Public Class TicketData
        Public Shared Function Create(description As String, body As String, creatorUserId As Guid) As Ticket
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "TICKETS_CREATE"

                cmd.Parameters.AddWithValue("@DESCRIPTION", description)
                cmd.Parameters.AddWithValue("@BODY", body)
                cmd.Parameters.AddWithValue("@CREATOR_USER_ID", creatorUserId)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).Rows(0)

                    Dim ticket As New Ticket
                    ticket.Id = CType(row("ID"), Guid)
                    ticket.Description = CStr(row("DESCRIPTION"))
                    ticket.CreatorUserId = CType(row("CREATOR_USER_ID"), Guid)
                    ticket.CreatedDate = CDate(row("CREATED_DATE"))
                    ticket.Status = CType(row("STATUS"), Ticket.TicketStatus)
                    ticket.ClaimedUserId = CType(If(IsDBNull(row("CLAIMED_USER_ID")), Nothing, row("CLAIMED_USER_ID")), Guid?)
                    ticket.ClosedDate = If(IsDBNull(row("CLOSED_DATE")), Nothing, CDate(row("CLOSED_DATE")))
                    ticket.SatisfactionRating = CInt(row("SATISFACTION_RATING"))
                    ticket.Body = CStr(row("BODY"))
                    ticket.Documents = Nothing

                    Return ticket
                End If
            End Using

            Return Nothing
        End Function

        Public Shared Function HasExistingTickets(userId As Guid) As Boolean
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "USERS_DOESHAVETICKET"

                cmd.Parameters.AddWithValue("@USER_ID", userId)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            End Using

            Return False
        End Function

        Public Shared Function GetData(ticketId As Guid) As Ticket
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "TICKETS_GETDETAIL"

                cmd.Parameters.AddWithValue("@TICKET_ID", ticketId)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables(0).Rows.Count > 0 Then
                    Dim row = ds.Tables(0).Rows(0)

                    Dim ticket As New Ticket
                    ticket.Messages = New List(Of Message)
                    ticket.Documents = New List(Of Document)

                    With ticket
                        .Id = CType(row("ID"), Guid)
                        .Description = CStr(row("DESCRIPTION"))
                        .CreatorUserId = CType(row("CREATOR_USER_ID"), Guid)
                        .CreatedDate = CDate(row("CREATED_DATE"))
                        .Status = CType(row("STATUS"), Ticket.TicketStatus)
                        .ClaimedUserId = If(IsDBNull(row("CLAIMED_USER_ID")), Nothing, CType(row("CLAIMED_USER_ID"), Guid?))
                        .ClaimedUserFullName = If(IsDBNull(row("CLAIMED_USER_FULLNAME")), Nothing, CStr(row("CLAIMED_USER_FULLNAME")))
                        .ClosedDate = If(IsDBNull(row("CLOSED_DATE")), Nothing, CType(row("CLOSED_DATE"), Date?))
                        .SatisfactionRating = CInt(row("SATISFACTION_RATING"))
                        .Body = CStr(row("BODY"))
                        .StatusName = CStr(row("STATUS_NAME"))
                        .CreatorFullName = CStr(row("CREATOR_FULLNAME"))
                    End With

                    If ds.Tables(1).Rows.Count > 0 Then
                        For Each docRow As DataRow In ds.Tables(1).Rows
                            Dim doc As New Document
                            doc.Id = CType(docRow("ID"), Guid)
                            doc.Description = CStr(docRow("DESCRIPTION"))

                            ticket.Documents.Add(doc)
                        Next
                    End If

                    If ds.Tables(2).Rows.Count > 0 Then
                        For Each msgRow As DataRow In ds.Tables(2).Rows
                            Dim msg As New Message
                            msg.Id = CType(msgRow("ID"), Guid)
                            msg.TicketId = CType(msgRow("TICKET_ID"), Guid)
                            msg.CreatorUserId = CType(msgRow("CREATOR_USER_ID"), Guid)
                            msg.CreatedDate = CDate(msgRow("CREATED_DATE"))
                            msg.Body = CStr(msgRow("BODY"))
                            msg.CreatorIsAdmin = CBool(msgRow("CREATOR_IS_ADMIN"))
                            msg.CreatorFullName = CStr(msgRow("CREATOR_FULLNAME"))

                            ticket.Messages.Add(msg)
                        Next
                    End If

                    Return ticket
                End If
            End Using

            Return Nothing
        End Function

        Public Shared Function AddMessage(ticketId As Guid, creatorUserId As Guid, body As String) As Message
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "TICKETS_MESSAGES_CREATE"

                cmd.Parameters.AddWithValue("@TICKET_ID", ticketId)
                cmd.Parameters.AddWithValue("@CREATOR_USER_ID", creatorUserId)
                cmd.Parameters.AddWithValue("@BODY", body)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).Rows(0)

                    Dim message = New Message
                    With message
                        .Id = CType(row("ID"), Guid)
                        .Body = CStr(row("BODY"))
                        .CreatedDate = CDate(row("CREATED_DATE"))
                        .CreatorUserId = CType(row("CREATOR_USER_ID"), Guid)
                        .TicketId = CType(row("TICKET_ID"), Guid)
                    End With

                    Return message
                End If
            End Using

            Return Nothing
        End Function
    End Class
End Namespace
