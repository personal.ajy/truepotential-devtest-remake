﻿Imports System.Data.SqlClient
Imports TP_Tickets.Models

Namespace Data
    Public Class RegistrationData
        Public Shared Function Create(forename As String, surname As String, login As String, password As String, Optional IsAdmin As Boolean = False) As User
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "USERS_CREATE"

                cmd.Parameters.AddWithValue("@FORENAME", forename)
                cmd.Parameters.AddWithValue("@SURNAME", surname)
                cmd.Parameters.AddWithValue("@LOGIN", login)
                cmd.Parameters.AddWithValue("@PASSWORD", password)
                cmd.Parameters.AddWithValue("@IS_ADMIN", IsAdmin)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables(0) IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).Rows(0)

                    Dim user As New Models.User
                    user.Id = CType(row("ID"), Guid)
                    user.Forename = CStr(row("FORENAME"))
                    user.Surname = CStr(row("SURNAME"))
                    user.Login = CStr(row("LOGIN"))
                    user.PasswordHashed = Nothing
                    user.IsAdmin = CBool(row("IS_ADMIN"))

                    Return user
                End If
            End Using

            Return Nothing
        End Function
    End Class
End Namespace
