﻿Imports System.Data.SqlClient
Imports System.Web.Configuration

Namespace Data
    Public Class DataBase

        'Private Shared ReadOnly Property ConnectionString As String = "Data Source=localhost;Database=TPDEVTEST;Integrated Security=True;"
        Private Shared ReadOnly Property ConnectionString As String
            Get
                Return System.Configuration.ConfigurationManager.ConnectionStrings("Main").ConnectionString
            End Get
        End Property

        Public Shared Function CreateConnection() As SqlConnection
            Return New SqlConnection(ConnectionString)
        End Function

    End Class
End Namespace


