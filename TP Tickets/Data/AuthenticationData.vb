﻿Imports System.Data.SqlClient

Namespace Data
    Public Class AuthenticationData
        Public Shared Function IsValidCredentials(login As String, password As String) As Boolean
            Using connection = DataBase.CreateConnection()
                Dim cmd As SqlCommand = connection.CreateCommand()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "AUTH_ISVALIDCREDENTIALS"

                cmd.Parameters.AddWithValue("@LOGIN", login)
                cmd.Parameters.AddWithValue("@PASSWORD", password)

                connection.Open()

                Dim ds As New DataSet
                Dim adapter As New SqlDataAdapter(cmd)
                adapter.Fill(ds)

                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return True
                End If
            End Using

            Return False
        End Function
    End Class
End Namespace
