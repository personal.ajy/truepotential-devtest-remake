﻿Public Class Validation
    Public Shared Function IsUserCreationValid(forename As String, surname As String, login As String, password As String) As Boolean
        If String.IsNullOrEmpty(forename) Then Return False
        If String.IsNullOrEmpty(surname) Then Return False
        If Not IsLoginValid(login) Then Return False
        If Not IsPasswordValid(password) Then Return False

        Return True
    End Function

    Public Shared Function IsPasswordValid(password As String) As Boolean
        If String.IsNullOrEmpty(password) Or password.Length < 10 And password.Length > 20 Then Return False

        Return True
    End Function

    Public Shared Function IsLoginValid(login As String) As Boolean
        If String.IsNullOrEmpty(login) Or login.Length >= 3 Then Return False

        Return True
    End Function

End Class
