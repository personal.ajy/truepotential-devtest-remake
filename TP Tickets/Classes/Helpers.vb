﻿Imports TP_Tickets.Models

Public Class Helpers
    Private Shared Property Session As HttpSessionState = HttpContext.Current.Session

    Public Class CurrentUser
        Public Shared ReadOnly Property Id As Guid
            Get
                Return CType(HttpContext.Current.Session("CurrentUserId"), Guid)
            End Get
        End Property

        Public Shared ReadOnly Property Forename As String
            Get
                Return CStr(HttpContext.Current.Session("CurrentUserForename"))
            End Get
        End Property

        Public Shared ReadOnly Property Surname As String
            Get
                Return CStr(HttpContext.Current.Session("CurrentUserSurname"))
            End Get
        End Property

        Public Shared ReadOnly Property Login As String
            Get
                Return CStr(HttpContext.Current.Session("CurrentUserLogin"))
            End Get
        End Property

        Public Shared ReadOnly Property IsAdmin As Boolean
            Get
                Dim info As UserDetails = Data.UserData.GetDetails(CurrentUser.Id)
                Return info.IsAdmin
            End Get
        End Property
    End Class

    Public Shared ReadOnly Property IsUserLoggedIn As Boolean
        Get
            Return CurrentUser.Id <> Nothing
        End Get
    End Property
End Class
