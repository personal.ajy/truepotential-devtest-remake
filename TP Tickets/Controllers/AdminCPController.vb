﻿Imports System.Net
Imports System.Web.Helpers
Imports System.Web.Mvc
Imports TP_Tickets.Models
Imports TP_Tickets.Models.ViewModels

Namespace Controllers
    <RoutePrefix("AdminCP")>
    Public Class AdminCPController
        Inherits Controller

        <Route>
        <Route("Index")>
        Function Index() As ActionResult
            Dim viewModel = New AdminCPViewModel With {
                .Tickets = Data.AdminCPData.GetTicketList()}

            Return View(viewModel)
        End Function

        <HttpGet>
        <Route("Ticket/Details/{ticketId}")>
        Function Details(ticketId As Guid) As ActionResult
            Dim viewModel As New Models.ViewModels.AdminCPTicketDetailsViewModel With {
                .Ticket = Data.TicketData.GetData(ticketId)}

            Return View(viewModel)
        End Function

        <HttpPost>
        <Route("Ticket/Claim/{ticketId}")>
        Function Claim(ticketId As Guid) As ActionResult
            Dim ticket As Ticket = Data.TicketData.GetData(ticketId)

            If ticket.ClaimedUserId Is Nothing Then
                Data.AdminCPData.ClaimTicket(ticketId, Helpers.CurrentUser.Id)

                Dim fb = New Models.ViewModels.FeedbackViewModel With {
                    .Text = "You have successfully claimed this ticket.",
                    .Type = FeedbackViewModel.FeedbackType.Success,
                    .Title = "Successfully claimed ticket",
                    .CanClose = True}

                Return PartialView("~/Views/Partials/Feedback.vbhtml", fb)
            Else
                Response.StatusCode = 500

                Dim fb = New Models.ViewModels.FeedbackViewModel With {
                    .Text = "This ticket has already been claimed.",
                    .Type = FeedbackViewModel.FeedbackType.Danger,
                    .Title = "Ticket already claimed",
                    .CanClose = True}

                Return PartialView("~/Views/Partials/Feedback.vbhtml", fb)
            End If
        End Function

        <HttpPost>
        <Route("Ticket/Release/{ticketId}")>
        Function Release(ticketId As Guid) As ActionResult
            Dim ticket = Data.TicketData.GetData(ticketId)

            If ticket.ClaimedUserId IsNot Nothing Then
                Data.AdminCPData.ReleaseTicket(ticketId)

                Dim fb = New Models.ViewModels.FeedbackViewModel With {
                    .Text = "You have successfully released this ticket.",
                    .Type = FeedbackViewModel.FeedbackType.Success,
                    .Title = "Successfully released ticket",
                    .CanClose = True}

                Return PartialView("~/Views/Partials/Feedback.vbhtml", fb)
            Else
                Response.StatusCode = 500

                Dim fb = New Models.ViewModels.FeedbackViewModel With {
                    .Text = "This ticket is not currently claimed.",
                    .Type = FeedbackViewModel.FeedbackType.Danger,
                    .Title = "Ticket not claimed",
                    .CanClose = True}

                Return PartialView("~/Views/Partials/Feedback.vbhtml", fb)
            End If
        End Function
    End Class
End Namespace