﻿Imports System.Net
Imports System.Web.Mvc
Imports TP_Tickets.Models
Imports TP_Tickets.Models.ViewModels
Namespace Controllers
    <RoutePrefix("Ticket")>
    Public Class TicketController
        Inherits Controller

        Function Index() As ActionResult
            Return View()
        End Function

        <HttpGet>
        <Route("Create")>
        Function Create() As ActionResult
            Return View()
        End Function

        <HttpPost>
        <Route("Create")>
        <ValidateInput(False)>
        Function Create(viewModel As CreateTicketViewModel) As ActionResult
            Dim validator As New CreateTicketViewModel.Validator
            Dim validationResult = validator.Validate(viewModel)

            If Not validationResult.IsValid Then
                TempData("Feedback") = New FeedbackViewModel With {
                    .Title = "Invalid Details",
                    .Text = "Please correct the below validation errors before continuing.",
                    .Type = FeedbackViewModel.FeedbackType.Danger,
                    .StrList = validationResult.Errors}

                Return View()
            Else
                Dim ticket As Ticket = Data.TicketData.Create(viewModel.Description, viewModel.Body, Helpers.CurrentUser.Id)

                TempData("Feedback") = New FeedbackViewModel With {
                        .Text = "Your ticket has been created successfully.",
                        .HtmlBody = String.Format("<hr/><b>Ticket ID:</b> {0}", ticket.Id.ToString()),
                        .Title = "Success",
                        .Type = FeedbackViewModel.FeedbackType.Success}

                Return View()
            End If
        End Function

        <HttpPost>
        <Route("AddMessage")>
        <ValidateInput(False)>
        Function AddMessage(ticketId As Guid, body As String) As HttpStatusCodeResult
            Data.TicketData.AddMessage(ticketId, Helpers.CurrentUser.Id, body)

            Return New HttpStatusCodeResult(HttpStatusCode.OK)
        End Function

        <HttpPost>
        <Route("GetMessagesPartialView")>
        Function GetMessagesPartialView(ticketId As Guid) As ActionResult
            Dim messages = Data.TicketData.GetData(ticketId).Messages

            Return PartialView("~/Views/Partials/TicketMessages.vbhtml", messages)
        End Function
    End Class
End Namespace