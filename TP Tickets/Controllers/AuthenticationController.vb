﻿Imports System.Web.Mvc
Imports TP_Tickets.Models

Namespace Controllers
    <RoutePrefix("Authentication")>
    Public Class AuthenticationController
        Inherits Controller

        <Route("~/Login")>
        <Route("Login")>
        Function Login() As ActionResult
            Return View("Login")
        End Function

        <HttpPost>
        <Route("~/Login")>
        <Route("Login")>
        Function Login(viewModel As Models.ViewModels.LoginViewModel) As ActionResult
            Dim validator As New Models.ViewModels.LoginViewModel.Validator
            Dim validationResult = validator.Validate(viewModel)

            If Not validationResult.IsValid Then
                Dim fb = New Models.ViewModels.FeedbackViewModel
                With fb
                    .Text = "Please ensure your login details are valid!"
                    .Title = "Error"
                    .StrList = validationResult.Errors
                    .Type = Models.ViewModels.FeedbackViewModel.FeedbackType.Danger
                End With
                TempData("Feedback") = fb

                Return View("Login")
            Else
                If Data.AuthenticationData.IsValidCredentials(viewModel.Login, viewModel.Password) Then
                    Dim userDetails As UserDetails = Data.UserData.GetDetails(login:=viewModel.Login)

                    FormsAuthentication.SetAuthCookie(viewModel.Login, True)
                    Session("CurrentUserId") = userDetails.Id
                    Session("CurrentUserForename") = userDetails.Forename
                    Session("CurrentUserSurname") = userDetails.Surname
                    Session("CurrentUserLogin") = userDetails.Login

                    If userDetails.IsAdmin Then
                        Return RedirectToAction("Index", "AdminCP")
                    Else
                        Dim hasTickets As Boolean = Data.TicketData.HasExistingTickets(userDetails.Id)
                        If Not hasTickets Then
                            TempData("Feedback") = New Models.ViewModels.FeedbackViewModel With {
                                .Text = "You have not previously created a ticket, or any of the tickets you may have created have been deleted. To continue, please start by creating a new ticket.",
                                .Type = ViewModels.FeedbackViewModel.FeedbackType.Info,
                                .Title = "Please create a ticket"}

                            Return RedirectToAction("Create", "Ticket")
                        Else
                            RedirectToAction("Login")
                        End If
                    End If
                Else
                    Dim fb = New Models.ViewModels.FeedbackViewModel
                    With fb
                        .Title = "Invalid Credentials"
                        .Text = "Sorry, but the credentials you provided are incorrect!"
                        .Type = Models.ViewModels.FeedbackViewModel.FeedbackType.Danger
                    End With
                    TempData("Feedback") = fb

                    Return View("Login")
                End If
            End If
        End Function

        <HttpGet>
        <Route("Logout")>
        <Route("~/Logout")>
        Function Logout() As ActionResult
            FormsAuthentication.SignOut()
            Session.Clear()

            Return RedirectToAction("Login", "Authentication")
        End Function
    End Class
End Namespace