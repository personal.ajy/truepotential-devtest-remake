﻿Imports System.Web.Mvc
Imports TP_Tickets.Models
Imports FluentValidation

Namespace Controllers
    <RoutePrefix("Registration")>
    Public Class RegistrationController
        Inherits Controller

        <Route()>
        <Route("~/Register")>
        <Route("Register")>
        Function Register() As ActionResult
            Return View()
        End Function

        <HttpPost>
        <Route("ActionRegister")>
        Function ActionRegister(viewModel As Models.ViewModels.RegistrationViewModel) As ActionResult
            Dim validator As New Models.ViewModels.RegistrationViewModel.Validator
            Dim validationResult = validator.Validate(viewModel)

            If Not validationResult.IsValid Then
                TempData("Feedback") = New Models.ViewModels.FeedbackViewModel With {
                    .Title = "Invalid Details",
                    .Text = "There appear to be issues with the details you have given!",
                    .Type = Models.ViewModels.FeedbackViewModel.FeedbackType.Danger,
                    .CanClose = False,
                    .StrList = validationResult.Errors}

                Return RedirectToAction("Register")
            Else
                Dim user As User = Data.RegistrationData.Create(viewModel.Forename, viewModel.Surname, viewModel.Login, viewModel.Password)

                If user IsNot Nothing Then
                    TempData("Feedback") = New Models.ViewModels.FeedbackViewModel With {
                        .Title = "Success",
                        .Text = "You have successfully created a user. You may now log in.",
                        .Type = ViewModels.FeedbackViewModel.FeedbackType.Success}

                    Return View("Register")
                End If
            End If
        End Function
    End Class
End Namespace