﻿Namespace Models
    Public Class Message
        Public Property Id As Guid

        Public Property TicketId As Guid

        Public Property Body As String

        Public Property CreatorUserId As Guid

        Public Property CreatedDate As Date

        Public Property CreatorIsAdmin As Boolean

        Public Property CreatorFullName As String
    End Class
End Namespace
