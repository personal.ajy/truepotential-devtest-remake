﻿Namespace Models
    Public Class User
        Public Property Id As Guid

        Public Property Login As String

        Public Property PasswordHashed As String

        Public Property CreatedDate As DateTime

        Public Property Forename As String

        Public Property Surname As String

        Public Property IsAdmin As Boolean
    End Class
End Namespace

