﻿Namespace Models
    Public Class Document
        Public Property Id As Guid

        Public Property TicketId As Guid

        Public Property Path As String

        Public Property Description As String

        Public Property UploadedDate As Date
    End Class
End Namespace
