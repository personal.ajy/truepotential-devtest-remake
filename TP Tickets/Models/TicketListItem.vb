﻿Namespace Models
    Public Class TicketListItem

        Public Property Id As Guid

        Public Property Description As String

        Public Property CreatedByName As String

        Public Property CreatedDate As Date

        Public Property ClaimedByName As String

        Public Property Status As Integer

        Public Property StatusName As String

    End Class
End Namespace
