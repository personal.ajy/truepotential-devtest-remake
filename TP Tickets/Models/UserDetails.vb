﻿Namespace Models
    <Serializable>
    Public Class UserDetails

        Public Property Id As Guid

        Public Property Forename As String

        Public Property Surname As String

        Public Property IsAdmin As Boolean

        Public Property Login As String

    End Class
End Namespace
