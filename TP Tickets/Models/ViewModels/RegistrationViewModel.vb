﻿Imports FluentValidation

Namespace Models.ViewModels
    <Attributes.Validator(GetType(RegistrationViewModel.Validator))>
    Public Class RegistrationViewModel
        Public Property Forename As String

        Public Property Surname As String

        Public Property Password As String

        Public Property PasswordRepeated As String

        Public Property Login As String

        Public Class Validator
            Inherits AbstractValidator(Of RegistrationViewModel)

            Public Sub New()
                RuleFor(Function(x) x.Forename).NotEmpty().Length(2, 50)
                RuleFor(Function(x) x.Surname).NotEmpty().Length(2, 50)
                RuleFor(Function(x) x.Password).NotEmpty().Length(5, 10)
                RuleFor(Function(x) x.PasswordRepeated).NotEmpty().Equal(Function(x) x.Password)
                RuleFor(Function(x) x.Login).NotEmpty().Length(5, 50)
            End Sub
        End Class
    End Class
End Namespace
