﻿Namespace Models.ViewModels
    Public Class FeedbackViewModel
        Public Enum FeedbackType
            Success
            Info
            Warning
            Danger
        End Enum

        Public Property Title As String

        Public Property Text As String

        Public Property Type As FeedbackType

        Public Property CanClose As Boolean = False

        Public Property StrList As IEnumerable

        Public Property HtmlBody As String
    End Class
End Namespace