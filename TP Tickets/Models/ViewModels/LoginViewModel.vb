﻿Imports FluentValidation

Namespace Models.ViewModels
    Public Class LoginViewModel
        Public Property Login As String

        Public Property Password As String

        Public Class Validator
            Inherits AbstractValidator(Of LoginViewModel)

            Public Sub New()
                RuleFor(Function(x) x.Login).NotEmpty()
                RuleFor(Function(x) x.Password).NotEmpty()
            End Sub
        End Class
    End Class
End Namespace
