﻿Imports FluentValidation

Namespace Models.ViewModels
    <Attributes.Validator(GetType(CreateTicketViewModel.Validator))>
    Public Class CreateTicketViewModel

        Public Property Description As String

        Public Property Body As String

        Public Class Validator
            Inherits AbstractValidator(Of CreateTicketViewModel)

            Public Sub New()
                RuleFor(Function(x) x.Description).NotEmpty().Length(3, 100)
                RuleFor(Function(x) x.Body).NotEmpty()
            End Sub
        End Class
    End Class
End Namespace
