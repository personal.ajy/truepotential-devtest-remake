﻿Imports TP_Tickets.Models

Namespace Models
    Public Class Ticket

        Public Enum TicketStatus
            [New] = 0
            Claimed = 1
            Resolved = 2
            Closed = 3
        End Enum

        Public Property Id As Guid

        Public Property Description As String

        Public Property Body As String

        Public Property CreatorUserId As Guid

        Public Property CreatorFullName As String

        Public Property CreatedDate As Date

        Public Property Status As TicketStatus

        Public Property StatusName As String

        Public Property ClaimedUserId As Guid?

        Public Property ClaimedUserFullName As String

        Public Property ClosedDate As Date?

        Public Property SatisfactionRating As Integer?

        Public Property Documents As List(Of Document)

        Public Property Messages As List(Of Message)

    End Class
End Namespace
